﻿$clientID = "decf3d88-faa9-452b-b8ff-be189ae749c9"
$key = "[tbyKJuDw***qdXxSftiijdkkQUK0906"
$SecurePassword = $key | ConvertTo-SecureString -AsPlainText -Force
$cred = new-object -typename System.Management.Automation.PSCredential `
-argumentlist $clientID, $SecurePassword
$tenantID = "0ab4cbbf-4bc7-4826-b52c-a14fed5286b9"

Connect-AzureRmAccount -Credential $cred -TenantId $tenantID -ServicePrincipal

$resourceGroupName = "darwinpoc";
$dataFactoryName = "darwinpoc-adf-SIT";

Set-AzureRmDataFactoryV2LinkedService -DataFactoryName 'darwinpoc-adf-SIT' AzureStorageLinkedService -ResourcegroupName darwinpoc -DefinitionFile ".\AzureStorageLinkedService.json" -force 
Set-AzureRmDataFactoryV2LinkedService -DataFactoryName 'darwinpoc-adf-SIT' AzureSqlDatabase1 -ResourcegroupName darwinpoc -DefinitionFile ".\AzureSqlDatabase1.json" -force 
Set-AzureRmDataFactoryV2Dataset -DataFactoryName 'darwinpoc-adf-SIT' AzureSqlTable1 -ResourcegroupName darwinpoc -DefinitionFile ".\AzureSqlTable1.json" –force
Set-AzureRmDataFactoryV2Pipeline -DataFactoryName 'darwinpoc-adf-SIT' darwinpocpipeline1 -ResourcegroupName darwinpoc -DefinitionFile ".\darwinpocpipeline1.json" –force




